v0.0.1
- [X] Basic README
- [X] Usable CLI (help, prompt, exit)
- [X] Support Debug mode
- [X] Organize Folder / Namespace
- [X] Add tests

v0.0.2
- [X] Binary Merkle tree

v0.0.3
- [X] Rename to BankX (BX Namespace)
- [x] Create umbrella app & split between: Client, Core, Crypto and Node

v0.0.4
- [ ] Broadcast transactions to mine
- [ ] Auto-mine transactions
- [ ] Publish on Hex

v0.0.5
- [ ] Serialize a block
- [ ] Simple Verification System

Later
- Re-organize client and split commands for nodes (Connect / start / stop / status) than for tokens (wallets, balances, send money, etc..) 
- Sessionlesss Client (ala heroku client)
- Merkle tree that supports any number of blocks not only a power of 2 (2^N) https://www.codeproject.com/Articles/1176140/Understanding-Merkle-Trees-Why-use-them-who-uses-t#ConsistencyProofCaveats17
- Define a Max CPU usage for Node
- Digitally sign transactions
- Recompose chain block per block (do not download whole chain)
- Introduce seed server
- Support node based on IP address (not just port)
