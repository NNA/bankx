defmodule BX.Mixfile do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      start_permanent: Mix.env == :prod,
      deps: deps(),
      # DOC
      name: "BankX",
      source_url: "https://gitlab.com/NNA/bankx",
      homepage_url: "http://YOUR_PROJECT_HOMEPAGE",
      docs: [ # main: "BankX",
              # logo: "path/to/logo.jpg",
              extras: ["README.md"]
            ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.16",          only: :dev, runtime: false},
      {:mix_test_watch, "~> 0.3",   only: :dev, runtime: false},
      {:dialyxir, "~> 0.5",         only: :dev, runtime: false},
      {:ex_unit_notifier, "~> 0.1", only: :dev}
    ]
  end
end
