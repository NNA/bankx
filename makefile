# Default target: build the project
default: build

clean:
	mix clean
	
build: clean
	(cd ./apps/bx_client; mix escript.build)

install:
	brew install erlang
	brew install elixir
	mix deps.get

tes:
	mix test

tdd:
	mix test.watch --stale

run:
	(cd ./apps/bx_client; ./bx_client)
