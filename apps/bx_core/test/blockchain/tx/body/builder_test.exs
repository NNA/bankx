defmodule BX.Core.TX.Body.BuilderTest do
  use ExUnit.Case, async: true
  alias BX.Core.TX.Body.Builder, as: Subject

  describe "build/1" do
    test "returns a BX.Core.TX.Body struct with given map" do
      body_map = %{amount: "amount", from: "from", to: "to"}
      body_struct = %BX.Core.TX.Body{amount: "amount", from: "from", to: "to"}

      assert Subject.build(body_map) == body_struct
    end
  end
end
