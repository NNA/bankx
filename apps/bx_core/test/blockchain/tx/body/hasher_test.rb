defmodule BX.Core.TX.Body.HasherTest do
  use ExUnit.Case, async: true
  alias BX.Core.TX.Body.Hasher, as: Subject

  describe "hash/1" do
    test "hashes from+to+amount" do
      tx_body_map = %{amount: "amount", from: "from", to: "to"}
      tx_body = %BX.Core.TX.Body{amount: "amount", from: "from", to: "to"}

      assert Subject.hash(tx_body) == BX.Crypto.Hash.encode("#{tx_body.from}#{tx_body.to}#{tx_body.amount}")
    end
  end
end
