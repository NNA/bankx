defmodule BX.Core.Block.CrafterTest do
  use ExUnit.Case, async: true
  alias BX.Core.Block.Crafter, as: Subject

  describe "genesis/0" do
    test "returns a genesis Block " do
      assert Subject.genesis() |> BX.Core.Block.Checker.genesis_block?()
    end
  end

  describe "next/1" do
    test "returns a Block with next index, previous block hash and dummy content" do
      block = %BX.Core.Block{
        index:            3,
        timestamp:        1_500_302,
        data:             [
          %BX.Core.TX{body: 'tx0', hash: 'hash_tx0'},
          %BX.Core.TX{body: 'tx1', hash: 'hash_tx1'}
        ],
        merkle_root_hash: "001122"
      }

      next = Subject.next(block)

      assert next.index               == 4
      assert next.data                == [
        %BX.Core.TX{body: 'tx0', hash: 'hash_tx0'},
        %BX.Core.TX{body: 'tx1', hash: 'hash_tx1'}
      ]
      assert next.previous_block_hash == "001122"
      assert next.merkle_root_hash    == "Elixir.MerkleTree.Mock root hash"
    end
  end

  describe "next/2" do
    test "returns a Block with next index, previous block hash and given content" do
      block = %BX.Core.Block{
        index:            3,
        timestamp:        1_500_302,
        data:             ["Block data"],
        merkle_root_hash: "001122"
      }

      next = Subject.next(
                            block,
                            [
                              %BX.Core.TX{body: 'tx1', hash: 'hash_tx1'},
                              %BX.Core.TX{body: 'tx2', hash: 'hash_tx2'}
                            ]
                          )

      # TODO Is there a way to check Struct without content ?
      # assert next             == %BX.Core.Block{}
      assert next.index               == 4
      assert next.data                == [
        %BX.Core.TX{body: 'tx1', hash: 'hash_tx1'},
        %BX.Core.TX{body: 'tx2', hash: 'hash_tx2'}
      ]
      assert next.previous_block_hash == "001122"
      assert next.merkle_root_hash    == "Elixir.MerkleTree.Mock root hash"
    end
  end

  describe "from_json_map/1" do
    # TODO
  end

end
