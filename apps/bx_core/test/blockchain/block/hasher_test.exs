defmodule BX.Core.Block.HasherTest do
  use ExUnit.Case, async: true
  alias BX.Core.Block.Hasher, as: Subject

  describe "hash/1" do
    test "returns a hash of concatenated index, data, nounce and previous_block_hash" do
      b = %BX.Core.Block{
        index:         3,
        timestamp:     1_500_302,
        data:          "Block data",
        nounce:        "001122",
        previous_block_hash: "previous_block_hash"
      }
      expected = BX.Crypto.Hash.encode("#{b.index}#{b.timestamp}#{b.nounce}#{b.previous_block_hash}")
      result   = Subject.hash(b)
      assert result == expected
    end
  end
end
