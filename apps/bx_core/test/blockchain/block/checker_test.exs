defmodule BX.Core.Block.CheckerTest do
  use ExUnit.Case, async: true
  alias BX.Core.Block.Checker, as: Subject

  import ExUnit.CaptureIO

  describe "genesis_block?/1" do
    test "returns true if index, data and previous hash of given block say so" do
      genesis = %BX.Core.Block{
        index:          0,
        timestamp:      1_505_512_521,
        data:           "Genesis Block",
        previous_block_hash:  "0"}
      assert Subject.genesis_block?(genesis) == true
    end
    test "returns false otherwise" do
      random = %BX.Core.Block{
        index:          33,
        timestamp:      1_505_512_522,
        data:           "Some data",
        previous_block_hash:  "32"}
      assert Subject.genesis_block?(random) == false
    end
  end

  describe "adjacent_blocks?/2" do
    test "outputs Invalid index & returns false if indexs are not consecutives" do
      block_i0 = %BX.Core.Block{index: 0}
      block_i2 = %BX.Core.Block{index: 2}
      func = fn ->
        assert Subject.adjacent_blocks?(block_i0, block_i2) == false
      end
      assert capture_io( func ) =~ ~r/Invalid index/
    end

    test "outputs Invalid previoushash & returns false if previous_block_hash(n+1) != merkle_root_hash(n)" do
      previous = %BX.Core.Block{index: 0, merkle_root_hash: "previous"}
      block    = %BX.Core.Block{index: 1, previous_block_hash: "hash"}
      func = fn ->
        assert Subject.adjacent_blocks?(previous, block) == false
      end
      assert capture_io( func ) =~ ~r/Invalid previoushash/
    end

    test "returns true if index are consecutives and hashes do match" do
      previous = %BX.Core.Block{index: 0, merkle_root_hash: "hash"}
      block    = %BX.Core.Block{index: 1, previous_block_hash: "hash"}
      assert Subject.adjacent_blocks?(previous, block) == true
    end
  end

end
