defmodule BX.Core.POW.ComputerTest do
  use ExUnit.Case, async: true
  alias BX.Core.POW.Computer, as: Subject

  @required_attempts 2

  setup do
    {:ok, mock} = BX.Core.POW.Checker.Mock.start_link(@required_attempts)
    %{mock: mock}
  end

  describe "compute/1" do
    test "add the nounce to given block, then hash it
          and return the nounce representing the number of attempts
          before pow is considered valid" do

      genesis = BX.Core.Block.Crafter.genesis()
      nounce = Subject.compute_pow(genesis)

      assert nounce == @required_attempts
    end
  end
end
