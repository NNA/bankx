defmodule BX.Core.POW.CheckerTest do
  use ExUnit.Case, async: true
  alias BX.Core.POW.Checker, as: Subject

  describe "pow_valid?/1" do

    test "true if has at least 3 leading zero" do
      assert Subject.pow_valid?("000123")  == true
      assert Subject.pow_valid?("0000123") == true
    end

    test "false if 0, 1 or 2 leading zeros" do
      assert Subject.pow_valid?("123")   == false
      assert Subject.pow_valid?("0123")  == false
      assert Subject.pow_valid?("00123") == false
    end

  end
end
