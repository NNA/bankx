use Mix.Config

config :logger, level: :info

config :bx_core, pow_difficulty: 3
config :bx_core, pow_checker: BX.Core.POW.Checker.Mock
config :bx_core, mt_builder: MerkleTree.Mock
