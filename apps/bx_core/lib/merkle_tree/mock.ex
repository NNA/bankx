defmodule MerkleTree.Behaviour do
  @callback new(MerkleTree.blocks, (String.t -> String.t)) :: MerkleTree.t
end

defmodule MerkleTree.Mock do
  @behaviour MerkleTree.Behaviour

  def new(_blocks, _hash_function) do
    %{
      root: %{
        value: "#{__MODULE__} root hash"
      }
    }
  end

end
