defmodule BX.Core do
  @moduledoc """
  Documentation for BX.Core.
  """

  @doc """
  Hello world.

  ## Examples

      iex> BX.Core.hello
      :world

  """
  def hello do
    :world
  end
end
