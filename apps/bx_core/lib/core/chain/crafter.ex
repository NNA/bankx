defmodule BX.Core.Chain.Crafter do
  def add_n_blocks(anything, 0) do
    anything
  end

  def add_n_blocks([], n) do
    add_n_blocks([BX.Core.Block.Crafter.genesis()], n-1)
  end

  def add_n_blocks([element], n) do
    add_n_blocks([BX.Core.Block.Crafter.next(element) | [element]], n-1)
  end

  def add_n_blocks([head | tail], n) do
    add_n_blocks([BX.Core.Block.Crafter.next(head) | [head | tail]], n-1)
  end
end
