defmodule BX.Core.Chain.Printer do
  def print([head | tail]) do
    BX.Core.Block.Printer.print(head)
    print(tail)
  end

  def print([element]) do
    BX.Core.Block.Printer.print(element)
  end

  def print(_) do
  end
end
