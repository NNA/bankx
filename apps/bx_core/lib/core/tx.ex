defmodule BX.Core.TX do
  defstruct [:hash, :body]
end
