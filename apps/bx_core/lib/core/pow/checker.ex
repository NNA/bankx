defmodule BX.Core.POW.Checker do
  @pow_difficulty Application.fetch_env!(:bx_core, :pow_difficulty)

  defmodule Behaviour do
    @callback pow_valid?(string) :: boolean
  end

  def pow_valid?(pow) do
    String.starts_with?(pow, String.duplicate("0", @pow_difficulty))
  end
end
