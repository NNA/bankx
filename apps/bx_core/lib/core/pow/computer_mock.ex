defmodule BX.Core.POW.Computer.Mock do
  @behaviour BX.Core.POW.Computer.Behaviour

  def compute_pow( _block, nounce ) do
    nounce
  end

end
