defmodule BX.Core.POW.Computer do
  defmodule Behaviour do
    @callback compute_pow(%BX.Core.Block{}, integer) :: integer
  end

  @pow_checker Application.fetch_env!(:bx_core, :pow_checker)
  @behaviour BX.Core.POW.Computer.Behaviour

  def compute_pow( %BX.Core.Block{} = block ) do
    compute_pow(block, 0)
  end

  def compute_pow( %BX.Core.Block{} = block,
                   nounce ) do
    b = %{block | nounce: nounce}
    hash = BX.Core.Block.Hasher.hash(b)
    case @pow_checker.pow_valid?(hash) do
      true -> nounce
      _    -> compute_pow(block, nounce + 1)
    end
  end
end
