defmodule BX.Core.POW.Checker.Mock do
  use Agent

  def start_link(try_count) do
    Agent.start_link(fn -> try_count end, name: __MODULE__)
  end

  def pow_valid?(pow) do
    # IO.puts "Calling POW.Checker.Mock"
    if try_and_get_remainings_count() <= 0, do: true, else: false
  end

  defp try_and_get_remainings_count do
    Agent.get_and_update(__MODULE__, fn state -> {state, state - 1} end)
  end

end
