defmodule BX.Core.Block do
  defstruct [ :index, :timestamp, :data, :nounce, :previous_block_hash, :merkle_root_hash ]
  @typedoc """
    Type that represents Block struct.
  """
  @type t :: %BX.Core.Block{
    index:               integer,
    timestamp:           DateTime,
    data:                Map,
    nounce:              integer,
    previous_block_hash: binary,
    merkle_root_hash:    binary
  }
end
