defmodule BX.Core.TX.Encoder do
  @derive [Poison.Encoder]

  def encode(tx) do
    Poison.encode!(tx)
  end
end
