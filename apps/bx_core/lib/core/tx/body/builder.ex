defmodule BX.Core.TX.Body.Builder do
  def build map do
    struct!(
      %BX.Core.TX.Body{},
      from: map[:from],
      to: map[:to],
      amount: map[:amount]
    )
  end
end
