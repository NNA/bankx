defmodule BX.Core.TX.Body.Hasher do
  def hash(body) do
    hash(body.from, body.to, body.amount)
  end

  defp hash(from, to, amount) do
    BX.Crypto.Hash.encode("#{from}#{to}#{amount}")
  end
end
