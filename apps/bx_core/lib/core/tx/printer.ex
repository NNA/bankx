defmodule BX.Core.TX.Printer do
  def print [head | tail] do
    print(head)
    print(tail)
  end

  def print [tx] do
    IO.puts("Transaction : Hash #{tx.hash} Body #{tx.body}")
  end

  def print _ do
  end
end
