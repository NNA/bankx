defmodule BX.Core.TX.Body do
  defstruct [:from, :to, :amount]
end
