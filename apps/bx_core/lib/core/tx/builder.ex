defmodule BX.Core.TX.Builder do
  def build(map) do
    body = BX.Core.TX.Body.Builder.build(map)
    hash = BX.Core.TX.Body.Hasher.hash(body)
    struct!(
      %BX.Core.TX{},
      body: body,
      hash: hash
    )
  end
end
