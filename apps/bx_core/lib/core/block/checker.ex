defmodule BX.Core.Block.Checker do

  def genesis_block?(block = %BX.Core.Block{}) do
    block.index                === 0                 &&
    block.timestamp            === 1_505_512_521     &&
    block.data                 === "Genesis Block"   &&
    block.previous_block_hash  === "0"
  end

  def adjacent_blocks?(previous_block = %BX.Core.Block{},
                       block = %BX.Core.Block{}) do
    cond do
      (previous_block.index + 1 !== block.index) ->
        IO.puts "Invalid index : previous #{previous_block.index} new #{block.index}"
        false
      (previous_block.merkle_root_hash !== block.previous_block_hash) ->
        IO.puts "Invalid previoushash"
        false
      # TODO Change Block Structure hash should not be inside block
      # (BX.Core.Block.Hasher.hash(block) !== block.hash) ->
      #   IO.puts "Invalid hash for block"
      #   false
      true ->
        true
    end
  end

end
