defmodule BX.Core.Block.Hasher do
  def hash(block) do
    hash(block.index, block.timestamp, block.nounce, block.previous_block_hash)
  end

  defp hash(index, timestamp, nounce, previous_block_hash) do
    BX.Crypto.Hash.encode("#{index}#{timestamp}#{nounce}#{previous_block_hash}")
  end
end
