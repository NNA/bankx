defmodule BX.Core.Block.Crafter do
  @mt_builder Application.fetch_env!(:bx_core, :mt_builder)

  def genesis do
    data = "Genesis Block"
    new_block(0, 1_505_512_521, data, "0", BX.Crypto.Hash.encode(data), nil)
  end

  def next(block) do
    next_block(block, block.data)
  end

  def next(block, data) do
    next_block(block, data)
  end

  def next(block, data, nounce) do
    next_block(block, data, nounce)
  end

  def from_json_map(map) do
    struct!(%BX.Core.Block{},
      index:               map["index"],
      timestamp:           map["timestamp"],
      data:                map["data"],
      nounce:              map["nounce"],
      previous_block_hash: map["previous_block_hash"],
      merkle_root_hash:    map["merkle_root_hash"]
    )
  end

  defp new_block(index, timestamp, data, previous_block_hash, merkle_root_hash, nounce) do
    %BX.Core.Block{
      index:               index,
      timestamp:           timestamp,
      data:                data,
      previous_block_hash: previous_block_hash,
      merkle_root_hash:    merkle_root_hash,
      nounce:              nounce
    }
  end

  defp next_block(block, data, nounce \\ 0) do
    index               = block.index + 1
    timestamp           = System.system_time(:second)
    previous_block_hash = block.merkle_root_hash
    merkle_root_hash    = @mt_builder.new(Enum.map(data, fn(d) -> d.hash end), &BX.Crypto.Hash.encode/1).root.value
    new_block(index, timestamp, data, previous_block_hash, merkle_root_hash, nounce)
  end

end
