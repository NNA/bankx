defmodule BX.Core.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bx_core,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps(),

      # DOC
      name: "BX.Core",
      source_url: "https://gitlab.com/NNA/bankx",
      homepage_url: "http://YOUR_PROJECT_HOMEPAGE",
      docs: [
              extras: ["README.md"]
            ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {BX.Core.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  def deps do
    [
      {:merkle_tree, "~> 1.2.0"},
      {:poison, "~> 3.1"},
      {:bx_crypto, in_umbrella: true}
    ]
  end
end
