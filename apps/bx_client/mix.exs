defmodule BX.Client.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bx_client,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.5",
      escript: escript(),
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  def escript do
    [main_module: BX.Client]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {BX.Client.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:bx_node, in_umbrella: true}
    ]
  end
end
