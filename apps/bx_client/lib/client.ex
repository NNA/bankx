defmodule BX.Client do
  require Logger

  # @help_commands "Press t (transaction), m (mine), n (nodes), l (list blocks) or h (help ) then press Enter: "
  @help_commands """
    Commands:
      t : Add transaction
      m : Mine a block
      n : List known nodes
      l : List all blocks of chain
      h : List of available commands
      c : Start consensus
      q : Exit bankx
      -> Always press "Enter" afterwards
"""
  @prompt "bankx> "

  def main(args \\ []) do
    Logger.debug("BankX Client Started")
    args |> parse_args |> start_node
    command_loop()
  end

  def start_node [] do
    BX.Node.NodeStarter.start_node()
  end

  def start_node options do
    BX.Node.NodeStarter.start_node(options[:port] || BX.Node.Agents.NodeAgent.default_port())
  end

  def command_loop do
    command = IO.gets @prompt
    case String.slice(command, 0..-2) do
      "t" ->
        Logger.debug "Adding a new transaction"
        create_transaction()
      "m" ->
        Logger.debug "Mining a new block"
        mine_block()
      "n" ->
        Logger.debug "Listing found nodes"
        list_nodes()
      "l" ->
        Logger.debug "Listing all blocks of the chain"
        list_blocks()
      "c" ->
        Logger.debug "Start consensus"
        all_chains()
      "h" ->
        Logger.debug "Showing help"
        help_commands()
      "q" ->
        Logger.debug "Quitting..."
        System.halt(0)
      _ ->
        nil
    end
    IO.puts ""
    command_loop()
  end

  def mine_block do
    HTTPoison.start
    {:ok, res} = HTTPoison.get BX.Node.Node.url("/mine")
    IO.puts res.body
  end

  def create_transaction do
    HTTPoison.start
    Logger.debug "New transaction:"
    from    = String.slice(IO.gets("Enter origin of transaction (from):"), 0..-2)
    to      = String.slice(IO.gets("Enter destination of transaction (to):"), 0..-2)
    amount  = String.slice(IO.gets("Enter amount to transfer (amount):"), 0..-2)
    {:ok, res} = HTTPoison.post BX.Node.Node.url("/transaction"),
      "{\"from\": \"#{from}\", \"to\": \"#{to}\", \"amount\": \"#{amount}\"}",
      [{"Content-Type", "application/json"}]
    IO.puts res.body
  end

  def list_blocks do
    HTTPoison.start
    {:ok, res} = HTTPoison.get BX.Node.Node.url("/blocks")
    IO.puts res.body
  end

  def list_nodes do
    BX.Node.Agents.NodeListAgent.all() |> BX.Node.Network.NodeList.print()
  end

  def all_chains do
    BX.Node.Network.NodeList.all_chains() |> IO.inspect
    BX.Node.Network.NodeConsensus.consensus()
  end

  def help do
    IO.puts """
      Usage:
        ./bankx -p [port_number] -d

      Options:
        --port  (-p) Port number used for node server. Default is #{BX.Node.Agents.NodeAgent.default_port()}
        --debug (-d) Display debug Log messages
        --help  (-h) Show this help message

      Description:
        DemoChain A BlockChain demo.

      #{@help_commands}
    """
    System.halt(0)
  end

  def debug_mode do
    Logger.info "Setting debug mode"
    Logger.configure(level: :debug)
  end

  def help_commands do
    IO.puts @help_commands
  end

  defp parse_args(args) do
    { options, _ , _ } = OptionParser.parse(args,
      switches: [port: :integer, help: :boolean, debug: :boolean],
      aliases:  [p: :port, h: :help, d: :debug]
    )
    cond do
      options[:help]  === true -> help()
      options[:debug] === true -> debug_mode()
      true -> nil
    end
    options
  end
end
