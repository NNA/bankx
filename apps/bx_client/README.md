# BX.Client

## Purpose:
At startup, the CLI starts a blockchain node on a *port*. Then using the prompt, various command can be used to interact with the blockchain.

## Usage:
Start the node using CLI

  ```bash
  ./bankx # by default the CLI stars a node on 4001 port
  ```
  CLI has 2 arguments :
  [port]  : port number used by the node
  [debug] : to activate verbose mode

  ```bash
  ./bankx --port 4003 # or -p 4003
  ```

  Press h to display help message
  ```bash
  bankx> h
      Commands:
        t : Add transaction
        m : Mine a block
        n : List known nodes
        l : List all blocks of chain
        h : List of available commands
        c : Start consensus
        q : Exit bankx
        -> Always press "Enter" afterwards

  bankx>
  ```
