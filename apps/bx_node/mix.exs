defmodule BX.Node.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bx_node,
      version: "0.0.1",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.5",
      start_permanent: Mix.env == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {BX.Node.NodeStarter, []},
      applications: [:cowboy, :plug],
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cowboy, "~> 1.0.0"},
      {:plug, "~> 1.0"},
      {:poison, "~> 3.1"},
      {:httpoison, "~> 0.13"},
      {:ex_utils, "~> 0.1.7"},

      {:bx_core, in_umbrella: true}
    ]
  end
end
