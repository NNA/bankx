defmodule BX.Node do
  @moduledoc """
  Documentation for BX.Node.
  """

  @doc """
  Hello world.

  ## Examples

      iex> BX.Node.hello
      :world

  """
  def hello do
    :world
  end
end
