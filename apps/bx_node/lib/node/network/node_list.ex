defmodule BX.Node.Network.NodeList do
  require Logger

  @port_range      4001..4010
  @lookup_interval 10

  def lookup_nodes do
    Task.async(fn -> lookup() end)
  end

  def lookup do
    for port <- @port_range, do: process_node(port)
    :timer.sleep(:timer.seconds(@lookup_interval))
    lookup()
  end

  def print list do
    IO.inspect(list)
  end

  def process_node port do
    unless BX.Node.Node.current? port do
      maybe_add_node_in_list    port
      maybe_remove_node_in_list port
    end
  end

  def maybe_add_node_in_list port do
    if BX.Node.Node.alive?(port) && !BX.Node.Agents.NodeListAgent.present?(port) do
      Logger.debug "Node #{port} found! => will be added"
      BX.Node.Agents.NodeListAgent.add(port)
    end
  end

  def maybe_remove_node_in_list port do
    if !BX.Node.Node.alive?(port) && BX.Node.Agents.NodeListAgent.present?(port) do
      Logger.debug "Node #{port} not responding! => will be removed"
      BX.Node.Agents.NodeListAgent.remove(port)
    end
  end

  def all_chains do
    for port <- BX.Node.Agents.NodeListAgent.all() do
      HTTPoison.start
      {:ok, res} = HTTPoison.get BX.Node.Node.url(port, "/blocks")
      chain = Poison.decode!(res.body)
              |> Enum.map(fn(x) -> BX.Core.Block.Crafter.from_json_map(x) end)
      {port, chain}
    end
  end
end
