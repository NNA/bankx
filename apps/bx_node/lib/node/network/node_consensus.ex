defmodule BX.Node.Network.NodeConsensus do
  require Logger

  @consensus_interval 20

  def consensus do
    Task.async(fn -> do_consensus() end)
  end

  defp do_consensus do
    keep_longuest_chain(current_chain() ++ BX.Node.Network.NodeList.all_chains())
    :timer.sleep(:timer.seconds(@consensus_interval))
    do_consensus()
  end

  defp keep_longuest_chain chains do
    {port, size, chain} = longuest_chain(chains)
    if !BX.Core.Chain.Checker.chain_valid?(chain) do
      Logger.debug "Chain for #{port} is invalid => keeping current chain"
    else
      unless BX.Node.Node.current?(port) do
        Logger.info "Node #{port} has longuer valid chain (size #{size}) => replacing current bankx"
        BX.Node.Agents.ChainAgent.replace(chain)
      end
    end
  end

  defp longuest_chain chains do
    Enum.reduce(chains, {0, 0, []}, fn(chain, max) ->
      {_max_port, max_size, _max_blocks} = max
      {current_port, blocks}             = chain
      current_size = Enum.count(blocks)
      if current_size > max_size do
        {current_port, current_size, blocks}
      else
        max
      end
    end)
  end

  defp current_chain do
    [{BX.Node.Agents.NodeAgent.get(:port), BX.Node.Agents.ChainAgent.all()}]
  end

end
