defmodule BX.Node.Server.NodeRouter do
  require Logger

  use Plug.Router

  if Mix.env == :dev do
    use Plug.Debugger
  end

  plug :match
  plug Plug.Parsers,  parsers: [:json],
                      pass:  ["application/json"],
                      json_decoder: Poison
  plug :dispatch

  get "/ping" do
    # IO.puts "API - Ping received"
    send_resp(conn, 200, "pong")
  end

  get "/blocks" do
    Logger.debug "API - Sending BlockChain"

    conn
      |> put_resp_header("accept", "application/json")
      |> send_resp(200, Poison.encode!(BX.Node.Agents.ChainAgent.all()))

  end

  get "/mine" do
    Logger.debug "API - Mining new block"

    {mining_res, block} = BX.Node.Agents.ChainAgent.last()
                          |> BX.Node.Miner.BlockMiner.mine()

    if (mining_res == :ok) do
      BX.Node.Agents.ChainAgent.add(block)

      BX.Node.Agents.ChainAgent.all()
        |> BX.Core.Chain.Printer.print()

      send_resp(conn, 200, "Success!")
    else
      IO.puts block
      send_resp(conn, 200, "Nothing done")
    end

  end

  post "/transaction" do
    Logger.debug "API - New transaction received"

    conn.body_params
      |> ExUtils.Map.atomize_keys()
      |> BX.Core.TX.Builder.build()
      |> BX.Node.Agents.TXAgent.add()

    BX.Node.Agents.TXAgent.all()
      |> BX.Core.TX.Printer.print()

    send_resp(conn, 200, "Success!")
  end

  match _ do
    IO.puts "404..."
    send_resp(conn, 404, "oops")
  end
end
