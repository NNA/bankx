defmodule BX.Node.Agents.NodeListAgent do
  require Logger

  use Agent

  @name __MODULE__

  def start_link(_opts) do
    Agent.start_link(fn -> init() end, name: @name)
  end

  # def last do
  #   Agent.get(@name, fn list -> Enum.at(list, 0) end)
  # end
  #

  def present? node do
    Enum.member?(all(), node)
  end

  def add(node) do
    unless present?(node) do
      Logger.debug "Adding Node to List"
      Agent.update(@name, fn list -> [node | list] end)
    end
  end

  def remove(node) do
    if present?(node) do
      Logger.debug "Removing Node to List"

      Agent.update(@name, fn list -> Enum.filter(list, fn e -> e != node end) end)
    end
  end

  def all() do
    Agent.get(@name, fn list -> list end)
  end

  defp init() do
    Logger.debug "Initializing NodeList Agent"
    []
  end
end
