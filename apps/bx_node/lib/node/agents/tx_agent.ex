defmodule BX.Node.Agents.TXAgent do
  require Logger

  use Agent

  @name __MODULE__

  def start_link(_opts) do
    Agent.start_link(fn -> init() end, name: @name)
  end

  def last do
    Agent.get(@name, fn list -> Enum.at(list, 0) end)
  end

  def add(transaction) do
    Logger.debug "Adding tx to waiting list"
    Agent.update(@name, fn list -> [transaction | list] end)
  end

  def all() do
    Agent.get(@name, fn list -> list end)
  end

  def empty() do
    Agent.update(@name, fn _list -> [] end)
  end

  defp init() do
    Logger.debug "Initializing Tx store"
    []
  end
end
