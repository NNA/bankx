defmodule BX.Node.Agents.NodeAgent do
  require Logger

  use Agent

  @name         __MODULE__
  @default_port 4001

  def start_link(_opts) do
    Agent.start_link(fn -> init() end, name: @name)
  end

  def get(key) do
    Agent.get(@name, fn state ->
      Map.get(state, key)
    end)
  end

  def set(key, value) do
    Agent.update(@name, fn state ->
      Map.put(state, key, value)
    end)
  end

  def default_port() do
    @default_port
  end

  defp init() do
    Logger.debug "Initializing Node Agent"
    %{port: @default_port}
  end
end
