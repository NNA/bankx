defmodule BX.Node.Agents.ChainAgent do
  require Logger

  use Agent

  @name __MODULE__

  def start_link(_opts) do
    Agent.start_link(fn -> init() end, name: @name)
  end

  def last do
    Agent.get(@name, fn list -> Enum.at(list, 0) end)
  end

  def add(block) do
    Logger.debug "Adding block to BlockChain"
    Agent.update(@name, fn list -> [block | list] end)
  end

  def all() do
    Agent.get(@name, fn list -> list end)
  end

  def replace(chain) do
    Agent.update(@name, fn _existing_chain -> chain end)
  end

  defp init() do
    Logger.debug "Initializing BlockChain State"
    BX.Core.Chain.Crafter.add_n_blocks([], 1)
  end
end
