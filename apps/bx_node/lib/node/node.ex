defmodule BX.Node.Node do

  def alive? port do
    # IO.puts "Testing port #{port}"
    HTTPoison.start
    case HTTPoison.get("http://localhost:#{port}/ping") do
      {:ok, %HTTPoison.Response{status_code: 200, body: "pong"}} ->
        true
      {:ok, response } ->
        # IO.inspect(response)
        false
      {:error, %HTTPoison.Error{reason: reason}} ->
        # IO.inspect reason
        false
    end
  end

  def current? port do
    BX.Node.Agents.NodeAgent.get(:port) == port
  end

  def url(path) do
    node_url BX.Node.Agents.NodeAgent.get(:port), path
  end

  def url(port, path) do
    node_url port, path
  end

  defp node_url port, path do
    "http://localhost:#{port}" <> path
  end

end
