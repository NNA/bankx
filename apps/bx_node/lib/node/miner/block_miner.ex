defmodule BX.Node.Miner.BlockMiner do
  require Logger

  @address "yYhGj4bvDHPzTrzLD3MyWnOHLtRfcbey78cptyKHeRI="

  def mine(last_block) do
    Logger.debug "Mining in progress..."

    # Do not mine if nb of trasactions + 1 (mining fee) isn't a multiple of 2
    # (because unsupported by MerkelTree lib)
    txs = BX.Node.Agents.TXAgent.all()
    if rem(length(txs), 2) == 0 do
      {:error, "Not enough TXs, add one... and try to mine again"}
    else
      nounce = BX.Core.POW.Computer.compute_pow(last_block)

      BX.Core.TX.Builder.build(%{from: "network", to: @address, amount: "1"})
        |> BX.Node.Agents.TXAgent.add()

      new_block = BX.Core.Block.Crafter.next(
        last_block,
        BX.Node.Agents.TXAgent.all(),
        nounce
      )

      BX.Node.Agents.TXAgent.empty()

      {:ok, new_block}
    end
  end
end
