defmodule BX.Node.NodeStarter do
  use Application

  require Logger

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      # Define workers and child supervisors to be supervised
      {BX.Node.Agents.NodeAgent,     name: BX.Node.Agents.NodeAgent},
      {BX.Node.Agents.NodeListAgent, name: BX.Node.Agents.NodeListAgent},
      {BX.Node.Agents.ChainAgent,    name: BX.Node.Agents.ChainAgent},
      {BX.Node.Agents.TXAgent,       name: BX.Node.Agents.TXAgent}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: BX.Supervisor]
    Supervisor.start_link(children, opts)
  end

  def start_node do
    Logger.debug("Starting node on default port")
    start_server()
  end

  def start_node port do
    Logger.debug("Starting node on port #{port}")
    BX.Node.Agents.NodeAgent.set(:port, port)
    start_server()
    BX.Node.Network.NodeList.lookup_nodes()
  end

  defp start_server do
    Plug.Adapters.Cowboy.http(BX.Node.Server.NodeRouter, [], [port: BX.Node.Agents.NodeAgent.get(:port)])
    Logger.info "Node now listen on port #{BX.Node.Agents.NodeAgent.get(:port)}"
  end
end
