defmodule BX.Crypto.Hash do
  def encode(string) do
    # Base.encode64(:crypto.hash(:sha256, string))
    :sha256 |> :crypto.hash(string) |> Base.url_encode64()
  end
end
