defmodule BX.Crypto do
  @moduledoc """
  Documentation for Cryptography.
  """

  @doc """
  Hello world.

  ## Examples

      iex> BX.Crypto.hello
      :world

  """
  def hello do
    :world
  end
end
