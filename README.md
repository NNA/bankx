# BankX
## An Simple BlockChain written in Elixir

The idea is to demo a "working" but very naive blockchain implementation. All complexity needed for real world use cases (security, stability, performance, etc..) is out of scope.

**Warning** This project is for educational purposes only.
It's not secure, not complete, not stable, etc...

## Key principles:
- __Proof-Of-Work consensus__: Transactions are merged & hashed. The first node
that computes hash with conditions (ie: with a hash containing X heading zeros)
can add the block to the chain. All nodes sync to longuest chain.
- __Step by step operations__: Mining is performed manually.
- __Local-nodes__: Up to 9 nodes can be started. Local nodes only.

## Components
BankX is an Elixir umbrella project composed of 4 different apps:

1. A [core lib](apps/bx_core). Data structures & Functions for blocks, chains,
  Proof-Of-Work and Transactions

2. A [crypto lib](apps/bx_crypto). Crypto functions

3. A [node server](apps/bx_node). Serves & holds state of blockchain.
  Once started, the node serves client HTTP requests:
  - __List blocks__ (POST /blocks)
  - __Add a transaction__: (POST /transaction)
  - __Mine a new block__: (POST /mine)

  It also performs these tasks periodically :
  - Looks up for new local nodes by scanning 40XX ports on localhost
  see [here](apps/bx_node/network/node_list.ex).
  - Performs consensus with peer nodes by keeping longuest chain.

4. A [Client](apps/bx_client). A command-line application for starting a node and
  play step by step operations (create [t]ransactions, [m]ine blocks, [l]ist all blocks).

## Install

BankX has only 2 pre-requisites :
- OTP (>= 20)
- Elixir

If you are on OS-X running and have homebrew installed, download the source code and run:

```bash
make install
```

## Roadmap

See [Roadmap](roadmap.md)

## Inspirations:

- [Let’s Build the Tiniest Blockchain](https://medium.com/crypto-currently/lets-build-the-tiniest-blockchain-e70965a248b)
